


Pour ce TP, j'ai réalisé 2 pages web qui étaient à réaliser dans le TP6 et le TP5 : la page Accueil et la page Personnages sur une série de mon choix.

Pour ce site, je me suis efforcé de respecter les attentes et de rendre chaque page parfaitement responsive et correspondante au schéma donné dans l'énoncé des exercices.

J'ai utilisé le positionnement flex premièrement pour rendre mon header et mon footer responsive. Je leur ai mis un flex-basis de 100% pour que leur taille s'adapte bien. Pour la liste de navigation dans le header, j'ai mis un flex-grow de 1 pour qu'elle prenne bien toute la longueur et un flex-basis de 19% ( en comptant les marges ) pour que mes 5 liens s'affichent bien a la suite et soient responsive. 

Ensuite, j'ai mis le main à coté du aside en leur donnant chacun un flex-basis de 65% / 35% pour avec un flex-wrap:nowrap; pour qu'ils s'affichent sur la même "ligne".

Enfin, j'ai bien aligné le texte dans mon footer en mettant un nowrap obligeant les 2 paragraphes à être sur la même ligne et je leur ai mis chacun un flex-basis de 50%.

Pour le contenu du main de la page Accueil, je n'ai globalement pas eu besoin de flex pour le positionner. J'ai simplement donné des marges automatiques à gauche et à droite des images pour qu'elles se plaçent au milieu automatiquement peu importe la taille de la fenêtre.    
Pour le contenu du main de la page Personnages, j'ai utilisé la propriété float pour permettre aux images de bien se placer à droite ou à gauche du texte. 

N'hésitez donc pas à tester en version téléphone pour vérifier que les pages sont belles et bien responsives. Ces pages sont d'ailleurs liées les unes entre les autres avec des balises <a> qui sont dans le menu de navigation.



Lien de mon dépot gitlab : 


https://gitlab.com/KylianRIBEROU/site-web-serie_tp6_td5/-/tree/main

Les pages " Accueil " et " Personnages " ont été codées et mises en lien.